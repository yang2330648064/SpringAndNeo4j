package com.yang.stu.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement //激活事务管理器
public class Neo4jConfiguration {

}