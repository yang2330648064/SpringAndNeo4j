package com.yang.stu.pojo;

import lombok.*;
import org.springframework.data.neo4j.core.schema.*;

import java.io.Serializable;
import java.util.List;

@Node
@NoArgsConstructor
@ToString
public class Movie implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private Long released;

    @Property("tagline")
    private String tagLine;

    //设置关系并且指定方向
    @Relationship(type = "ACTED_IN",direction = Relationship.Direction.INCOMING)
    private List<ActedIn> actedIns;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getReleased() {
        return released;
    }

    public void setReleased(Long released) {
        this.released = released;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public List<ActedIn> getActedIns() {
        return actedIns;
    }

    public void setActedIns(List<ActedIn> actedIns) {
        this.actedIns = actedIns;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", released=" + released +
                ", tagLine='" + tagLine + '\'' +
                ", actedIns=" + actedIns +
                '}';
    }
}