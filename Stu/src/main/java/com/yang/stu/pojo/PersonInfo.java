package com.yang.stu.pojo;

import lombok.Data;

import java.util.List;
@Data
public class PersonInfo {
    private String name;
    private Integer age;
    private List<Movie> movies;

}
