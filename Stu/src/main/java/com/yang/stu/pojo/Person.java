package com.yang.stu.pojo;


import org.springframework.data.neo4j.core.schema.*;
import java.io.Serializable;
import java.util.List;

@Node
public class Person implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Long born;

    private String sex;

    @Relationship(type = "ACTED_IN",direction = Relationship.Direction.OUTGOING)
    private List<ActedIn> actedIns;

    //导演关系
    @Relationship(type = "DIRECTED")
    private List<Movie> directedMovies;

    //出品关系
    @Relationship(type = "PRODUCED")
    private List<Movie> producedMovies;

    //写作关系
    @Relationship(type = "WROTE")
    private List<Movie> wroteMovies;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBorn() {
        return born;
    }

    public void setBorn(Long born) {
        this.born = born;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<ActedIn> getActedIns() {
        return actedIns;
    }

    public void setActedIns(List<ActedIn> actedIns) {
        this.actedIns = actedIns;
    }

    public List<Movie> getDirectedMovies() {
        return directedMovies;
    }

    public void setDirectedMovies(List<Movie> directedMovies) {
        this.directedMovies = directedMovies;
    }

    public List<Movie> getProducedMovies() {
        return producedMovies;
    }

    public void setProducedMovies(List<Movie> producedMovies) {
        this.producedMovies = producedMovies;
    }

    public List<Movie> getWroteMovies() {
        return wroteMovies;
    }

    public void setWroteMovies(List<Movie> wroteMovies) {
        this.wroteMovies = wroteMovies;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", born=" + born +
                ", sex='" + sex + '\'' +
                ", actedIns=" + actedIns +
                ", directedMovies=" + directedMovies +
                ", producedMovies=" + producedMovies +
                ", wroteMovies=" + wroteMovies +
                '}';
    }
}
