package com.yang.stu.service;
import com.yang.stu.pojo.Person;
import com.yang.stu.repository.PersonRepository;
import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class PersonService {
    @Resource
    private PersonRepository personRepository;

    @Transactional //确保操作的原子性
    public void addSexToAllPerson(){
        List<Person> personList = personRepository.findAll();
        String[] sexs = {"man", "woman"};
        for (Person person : personList) {
            person.setSex(sexs[new Random().nextInt(2)]);
            personRepository.save(person);
        }
    }
}
