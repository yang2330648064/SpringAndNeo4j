package com.yang.stu.repository;

import com.yang.stu.pojo.ActedIn;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActedRepository extends Neo4jRepository<ActedIn,Long> {

}
