package com.yang.stu.repository;

import com.yang.stu.pojo.Person;
import com.yang.stu.pojo.PersonInfo;
import org.neo4j.driver.Result;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends Neo4jRepository<Person,Long> {
    Person findByName(String name);

    List<Person> findByNameContains(String tom);

    @Query("match (m:Movie) <-[:ACTED_IN]-(p) where m.title=$movieName return p")
    List<Person> findByMovie(@Param("movieName") String movieName);

    @Query("match (n:Person {name:$name})-[:ACTED_IN]->(x) return n.name as name,$year-n.born as age,collect(x) as movies")
    PersonInfo findAgeAndMovieCountByName(@Param("name") String name, @Param("year") Integer year);
}
