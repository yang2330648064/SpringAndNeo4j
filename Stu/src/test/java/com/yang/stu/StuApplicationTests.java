package com.yang.stu;

import com.yang.stu.pojo.Movie;
import com.yang.stu.pojo.Person;
import com.yang.stu.pojo.PersonInfo;
import com.yang.stu.repository.MovieRepository;
import com.yang.stu.repository.PersonRepository;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;


@SpringBootTest
class StuApplicationTests {
	@Autowired
	private PersonRepository personRepository;

	@Resource
	private MovieRepository movieRepository;

	@Test
	public void existPersonById() {
		boolean exists = personRepository.existsById(34L);
		System.out.println(exists);
	}

	@Test
	public void queryPersonAll() {
		List<Person> personList = personRepository.findAll();
		personList.forEach(x-> System.out.println(x.toString()));
	}

	@Test
	public void queryPersonById() {
		Optional<Person> optionalPerson = personRepository.findById(34L);
		System.out.println(optionalPerson.get().getName());
	}


	@Test
	public void queryPersonByName() {
		String name = "Tom Hanks";  // 假设我们要查询的名字是“张三”
		Person person = personRepository.findByName(name);
		System.out.println(person);
	}

	@Test
	public void queryMovieById() {
		Optional<Movie> optionalMovie = movieRepository.findById(32L);
		System.out.println(optionalMovie.get().getTitle());
	}

	@Test
	public void findByNameContains() {
		List<Person> personList = personRepository.findByNameContains("Tom");
		personList.forEach(x-> System.out.println(x.toString()));
	}

	@Test
	public void testFindPage(){
		PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Order.asc("name")));
		Page<Person> page = personRepository.findAll(pageRequest);
		List<Person> personList = page.getContent();
		System.out.println("数据总量 :"+page.getTotalElements());
		System.out.println("总页数 :"+page.getTotalPages());
		personList.forEach(x-> System.out.println(x.toString()));
	}

	@Test
	public void fineActorsByMovieName(){
		List<Person> actors = personRepository.findByMovie("Johnny Mnemonic");
		actors.forEach(x-> System.out.println(x.toString()));
	}

	@Test
	public void findAgeAndMovieCountByName(){
		PersonInfo result = personRepository.findAgeAndMovieCountByName("Tom Hanks", 2025);
		System.out.println(result);
	}

	@Test
	public void savePerson(){
		Person person = new Person();
		person.setName("沈崇");
		person.setBorn(1980L);
		Person savedPerson = personRepository.save(person);
		System.out.println(savedPerson);
	}

	@Test
	public void SaveMovie(){
		Movie movie = new Movie();
		movie.setTitle("西红柿首富");
		movie.setReleased(2019L);
		movie.setTagLine("搞笑");

		Movie savedMovie = movieRepository.save(movie);
		System.out.println(savedMovie);
	}

	@Test
	public void findByName(){
		Movie movie = movieRepository.findByTitle("西红柿首富");
		System.out.println(movie);
	}

	@Test
	public void UpdataMovie(){
		Movie movie = movieRepository.findByTitle("西红柿首富");
		movie.setReleased(2018L); //更新值
		Movie saved = movieRepository.save(movie);
		System.out.println(saved);
	}

	/**
	 * 根据id删除
	 */
	@Test
	public void deleteMovieById(){
		movieRepository.deleteById(32L);
	}

	/**
	 * 根据实体删除
	 */
	@Test
	public void deleteMovieByTitle(){
		Movie movie = movieRepository.findByTitle("西红柿首富");
		movieRepository.delete(movie);
	}
}
