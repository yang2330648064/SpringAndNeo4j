package com.yang.stu;

import com.yang.stu.pojo.ActedIn;
import com.yang.stu.pojo.Movie;
import com.yang.stu.pojo.Person;
import com.yang.stu.repository.ActedRepository;
import com.yang.stu.repository.MovieRepository;
import com.yang.stu.repository.PersonRepository;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ActedInRepositoryTest {
    @Resource
    private ActedRepository actedRepository;

    @Resource
    private PersonRepository personRepository;

    @Resource
    private MovieRepository movieRepository;

    @Test
    public void saveActed() {
        // 查询 Person 和 Movie 节点
        Person person = personRepository.findByName("沈崇");
        Movie movie = movieRepository.findByTitle("西红柿首富");

        // 创建关系
        ActedIn actedIn = new ActedIn();
        actedIn.setPerson(person);
        actedIn.setMovie(movie);
        actedIn.setRoles(new String[]{"王多鱼"});

        // 将 ActedIn 添加到 Person 的关系列表中
        List<ActedIn> actedIns = person.getActedIns();
        actedIns.add(actedIn);
        person.setActedIns(actedIns);

        // 保存 Person，实现级联保存
        personRepository.save(person);
    }

    @Test
    public void saveAll(){
        Movie movie = movieRepository.findByTitle("西红柿首富");
        List<Person> personList=new ArrayList<>();
        Person person = new Person();
        person.setName("艾伦");
        person.setBorn(1981L);

        ActedIn actedIn = new ActedIn();
        actedIn.setPerson(person);
        actedIn.setMovie(movie);
        actedIn.setRoles(new String[]{"高然"});

        person.setActedIns(Arrays.asList(actedIn));
        personList.add(person);

        Person person2 = new Person();
        person2.setName("张一鸣");
        person2.setBorn(1986L);

        ActedIn actedIn2 = new ActedIn();
        actedIn2.setPerson(person2);
        actedIn2.setMovie(movie);
        actedIn2.setRoles(new String[]{"庄强"});

        person2.setActedIns(Arrays.asList(actedIn2));

        personList.add(person2);

        List<Person> saveAll = personRepository.saveAll(personList);
        saveAll.forEach(System.out::println);

    }
}
